<?php

namespace Oleg\SolomonoV2\Modals;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'product';

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

//    public function characteristics()
//    {
//        return $this->belongsToMany(Characteristic::class, 'product_characteristic', 'product_id', 'characteristic_id');
//    }
    public function characteristics()
    {
        return $this->belongsToMany(Characteristic::class, 'product_characteristic');
    }
}
