<?php

namespace Oleg\SolomonoV2;

use FastRoute\RouteCollector;
use FastRoute\Dispatcher;
use Illuminate\Database\Capsule\Manager as DatabaseManager;
use Oleg\SolomonoV2\App\Log\Builder;
use Oleg\SolomonoV2\View\LatteView;
use function FastRoute\simpleDispatcher;
use Oleg\SolomonoV2\View\BaseView;
use Dotenv\Dotenv;

class Application
{
    public Dispatcher $dispatcher;

    public function init()
    {
        $this->initDotenv();
        $this->initDispatcher();
        $this->initDatabase();
    }

    private function initDispatcher(): void
    {
        $routes = require_once __DIR__.'/../config/routes.php';
        $this->dispatcher = simpleDispatcher(function(RouteCollector $r) use ($routes) {
            foreach ($routes as $route) {
                $r->addRoute($route['http_method'], $route['route'], $route['handler']);
            }
        });
    }

    private function initDotenv(): void
    {
        // Загружаем переменные окружения из файла .env
        $dotenv = Dotenv::createImmutable(__DIR__.'/../..');
        $dotenv->load();
    }

    private function initDatabase(): void
    {
        $settings = require_once __DIR__.'/../config/database.php';
        $databaseManager = new DatabaseManager;
        $databaseManager->addConnection([
            "driver" => $settings['driver'],
            "host" => $settings['host'],
            "database" => $settings['database'],
            "username" => $settings['username'],
            "password" => $settings['password']
        ]);
        $databaseManager->setAsGlobal();
        $databaseManager->bootEloquent();
    }

    private function initDependencies(LatteView $view, $dependencies): void
    {
        foreach ($dependencies as $key => $dependencyList) {
            foreach ($dependencyList as $dependency) {
                $view->addDependency($dependency, $key);
            }
        }
    }

    public function execute()
    {
        try {
            $this->logHit();
            // Получаем метод запроса и сам URL
            $httpMethod = $_SERVER['REQUEST_METHOD'];
            $uri = $_SERVER['REQUEST_URI'];

            // Проверяем, есть ли GET параметры
            if (false !== $pos = strpos($uri, '?')) {
                $uri = substr($uri, 0, $pos);
            }

            // Декодируем URL
            $uri = rawurldecode($uri);

            // Берём информацию о роутинге
            $routeInfo = $this->dispatcher->dispatch($httpMethod, $uri);
            // Проверяем
            switch ($routeInfo[0]) {
                // Если нет страницы
                case Dispatcher::NOT_FOUND:
                    // ... 404 Не найдена страница
                    break;
                // Если нет метода для обработки
                case Dispatcher::METHOD_NOT_ALLOWED:
                    $allowedMethods = $routeInfo[1];
                    // ... 405 Нет метода
                    break;
                // Если всё нашлось
                case Dispatcher::FOUND:
                    $handler = $routeInfo[1];
                    $vars = $routeInfo[2];
                    //магия пхп. аналог вызова функции (т.е нам приходит функция в $handler , и чтобы ее вызвать мы используем
                    // как вызов обычноый функции $handler();
                    $controller = new $handler['class'];
                    $method = $handler['method'];

                    /**@var BaseView $view*/

                    $view = $controller->$method($vars);
                    if ($view instanceof LatteView) {
                        $this->initDependencies($view, $handler['dependencies'] ?? []);
                    }
                    $view->renderContent();
                    echo $view->getContent();
                    break;
            }
        } catch (\Throwable $e) {
            $this->logError($e);
        }
    }

    private function logHit(): void
    {
        $logger = Builder::getInstance()->getLogger('hit');
        $logger->info($_SERVER['REQUEST_URI']);
    }

    private function logError(\Throwable $e): void
    {
        $logger = Builder::getInstance()->getLogger('error');
//        $trace = $e->getTraceAsString();
//        $message = $e->getMessage() . "\nTrace:\n" . $trace;
        $logger->error($e->getMessage(), $e->getTrace());
    }
}