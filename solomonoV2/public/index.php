<?php
//загружаем composer autoload(автозагрущик класса)
require __DIR__.'/../vendor/autoload.php';

$application = new \Oleg\SolomonoV2\Application();
$application->init();
$application->execute();
