# SolomonoV2

SolomonoV2 - это веб-приложение для управления каталогом товаров.

## Установка и настройка

1. **Клонирование репозитория**
- `git clone https://gitlab.com/nozdrachov.oleg/solomonov2.git`

2. **Настройка Docker**

Установите [Docker](https://www.docker.com/) и [Docker Compose](https://docs.docker.com/compose/), если у вас их еще нет.

3. **Настройка переменных окружения**

Создайте файл `.env` в корне проекта и добавьте следующие строки:
- `MYSQL_USER=`
- `MYSQL_PASSWORD=`
- `MYSQL_ROOT_PASSWORD=`

Заполните значения переменных окружения в соответствии с вашими настройками.

4. **Запуск контейнеров Docker**
- `docker compose up --build -d`

5. **Накатите миграцию базы данных**

Накатите миграцию базы данных с помощью следующей команды(`php-cli-container` и `solomonoV2/cli/11-04-2023-base-structure-migration.php` - может быть каким вы захотите) :
- `docker compose run --rm php-cli-container php solomonoV2/cli/11-04-2023-base-structure-migration.php up`

6. **Проверьте подключение к базе данных**
- если у вас не получается , попробуйте удалить `невидимого пользователя - (потушите контейнера  , а не образы`

7. **Проверка работы приложения**

Откройте веб-браузер и перейдите по адресу `http://localhost`. Вы должны увидеть рабочее приложение.
