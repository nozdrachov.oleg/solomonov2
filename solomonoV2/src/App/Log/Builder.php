<?php

namespace Oleg\SolomonoV2\App\Log;

use Exception;
use Monolog\Logger;
use Oleg\SolomonoV2\App\Helpers\Singleton;
//НАПОМИНАНИЕ!! ПОЛНЫЙ ПУТЬ В КОНФИГ. ФАЙЛЕ! по этому use  не нужен!
//use Monolog\Handler\StreamHandler;

class Builder extends Singleton
{
    public const SOURCE_FILE = 'file';
    public const SOURCE_DATABASE = 'database';

    /**
     * @throws Exception
     */
    public function getLogger(string $name): Logger
    {
        $loggerConfig = require __DIR__.'/../../../config/logger.php';
        $source = env('LOG_SOURCE');
        $logger = new Logger($name);

        if (isset($loggerConfig[$source])) {
            $class = $loggerConfig[$source]['class'];
            switch ($source) {
                case self::SOURCE_FILE:
                    $handler = new $class("{$loggerConfig[$source]['path']}/{$name}.log");
                    break;
                case self::SOURCE_DATABASE:
                    $handler = new $class(str_replace('{name}', $name, $loggerConfig[$source]['table_template']));
                    break;
                default:
                    throw new Exception('LOG_SOURCE not found');
            }
            $logger->pushHandler($handler);
        }
        return $logger;
    }
}