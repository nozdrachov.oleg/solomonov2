<?php
return [
    "driver" => "mysql",
    "host" => "mysql-container",
    "database" => env('MYSQL_DATABASE'),
    "username" => env('MYSQL_USER'),
    "password" => env('MYSQL_PASSWORD')
];
