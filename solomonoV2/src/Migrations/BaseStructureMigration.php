<?php

namespace Oleg\SolomonoV2\Migrations;

use Exception;
use Illuminate\Database\Capsule\Manager as DatabaseManager;
use Illuminate\Support\Facades\Date;

class BaseStructureMigration
{
    /**
     * @param array $parameters
     * @return array
     * @throws Exception
     */
    public static function parseParameters(array $parameters): array
    {
        switch (count($parameters)){
            case 2:
                $result = array_combine(['path', 'action'], $parameters);
                break;
            case 0:
            case 1:
            default:
                throw new Exception('no valid parameters');
        }
        return  $result;
    }

    public function __construct()
    {
        $databaseManager = new DatabaseManager;
        $databaseManager->addConnection([
            "driver" => "mysql",
            "host" =>"mysql-container",
            "database" => env('MYSQL_DATABASE'),
            "username" => env('MYSQL_USER'),
            "password" => env('MYSQL_PASSWORD')
        ]);
        $databaseManager->setAsGlobal();
        $databaseManager->bootEloquent();
    }

    public static function showMessage(string $message): void
    {
        echo $message . PHP_EOL;
        //выплевывает данные в консоль
        ob_flush();
    }

    public function execute(array $parameters = []): void
    {
        if (empty($parameters['action'])) {
            throw new Exception('action parameter missing!');
        }
        $action = $parameters['action'];
        $this->$action();
    }

    public function up(): void
    {
        $this->createUserTable();
        $this->createProductTable();
        $this->createCategoryTable();
        $this->createProductDescriptionTable();
        $this->createProductCharacteristicTable();
        $this->createBasketItemTable();
        $this->createCharacteristicTable();
        $this->fillUserTable();
        $this->fillProductTable();
        $this->fillCategoryTable();
        $this->fillProductDescriptionTable();
        $this->fillProductCharacteristicTable();
        $this->fillBasketItemTable();
        $this->fillCharacteristicTable();
    }

    public function createUserTable()
    {
        // Создание таблицы users
        DatabaseManager::schema()->create('users', function ($table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('email', 50)->unique();
            $table->string('password', 100);
        });
        self::showMessage('Создание таблицы users - успешно!');
    }

    public function createProductTable()
    {
        // Создание таблицы product
        DatabaseManager::schema()->create('product', function ($table) {
            $table->bigIncrements('id');
            $table->string('name', 150);
            $table->unsignedFloat('price');
            $table->timestamps();
            $table->unsignedInteger('category_id');
            $table->enum('status', ['active', 'inactive']);
        });
        self::showMessage('Создание таблицы product - успешно!');
    }

    public function createCategoryTable()
    {
        // Создание таблицы category
        DatabaseManager::schema()->create('category', function ($table) {
            $table->bigIncrements('id');
            $table->string('name', 100);
            $table->string('code', 100);
        });
        self::showMessage('Создание таблицы category - успешно!');
    }

   public function createProductDescriptionTable()
   {
       // Создание таблицы product_description
       DatabaseManager::schema()->create('product_description', function ($table) {
           $table->bigIncrements('id');
           $table->unsignedInteger('product_id');
           $table->text('description');
       });
       self::showMessage('Создание таблицы product_description - успешно!');
   }

   public function createProductCharacteristicTable()
   {
       // Создание таблицы product_characteristic
       DatabaseManager::schema()->create('product_characteristic', function ($table) {
           $table->bigIncrements('id');
           $table->unsignedInteger('product_id');
           $table->unsignedInteger('characteristic_id');
       });
       self::showMessage('Создание таблицы product_characteristic - успешно!');
   }

   public function createBasketItemTable()
   {
       // Создание таблицы basket_item
       DatabaseManager::schema()->create('basket_item', function ($table) {
           $table->bigIncrements('id');
           $table->unsignedInteger('user_id');
           $table->unsignedFloat('price');
           $table->unsignedBigInteger('count');
           $table->string('user_session', 255);
           $table->unsignedInteger('product_id');
       });
       self::showMessage('Создание таблицы basket_item - успешно!');
   }

    public function createCharacteristicTable()
    {

        // Создание таблицы characteristic
        DatabaseManager::schema()->create('characteristic', function ($table) {
            $table->bigIncrements('id');
            $table->string('code', 100);
            $table->string('name', 50);
            $table->string('value', 100);
        });
        self::showMessage('Создание таблицы characteristic - успешно!');
    }


    public function fillUserTable()
    {
        // Вставка данных в таблицу users
        DatabaseManager::table('users')->insert([
            'name' => 'John Doe',
            'email' => 'john@example.com',
            'password' => 'password123',
        ]);
        self::showMessage('Наполнение таблицы users - успешно!');
    }

    public function fillProductTable()
    {
        // Вставка данных в таблицу product
        DatabaseManager::table('product')->insert([
            [
                'name' => 'Apple MacBook A1534',
                'price' => 1000,
                'category_id' => 1,
                'status' => 'active',
                'created_at' => Date::now(),
                'updated_at' => Date::now()

            ],
            [
                'name' => 'Apple MacBook Pro',
                'price' => 3300,
                'category_id' => 1,
                'status' => 'inactive',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Lenovo Yoga 3 Pro',
                'price' => 1330,
                'category_id' => 1,
                'status' => 'inactive',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Sony Vaio Flip 15',
                'price' => 1070,
                'category_id' => 1,
                'status' => 'active',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Meizu M5',
                'price' => 337,
                'category_id' => 2,
                'status' => 'active',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Apple iPhone 6s',
                'price' => 1450,
                'category_id' => 2,
                'status' => 'active',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Apple iPhone SE',
                'price' => 561,
                'category_id' => 2,
                'status' => 'inactive',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Xiaomi Redmi 4A',
                'price' => 119,
                'category_id' => 2,
                'status' => 'active',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Xiaomi Mi6',
                'price' => 480,
                'category_id' => 2,
                'status' => 'inactive',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Xiaomi Mi A1',
                'price' => 225,
                'category_id' => 2,
                'status' => 'inactive',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Apple Watch Series 7',
                'price' => 416,
                'category_id' => 3,
                'status' => 'active',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
            [
                'name' => 'Apple Watch Series 6',
                'price' => 416,
                'category_id' => 3,
                'status' => 'active',
                'created_at' => Date::now(),
                'updated_at' => Date::now()
            ],
        ]);
        self::showMessage('Наполнение таблицы product - успешно!');
    }

    public function fillCategoryTable()
    {
        // Вставка данных в таблицу category
        DatabaseManager::table('category')->insert([
            ['name' => 'Laptops', 'code' => 'laptops'],
            ['name' => 'Smartphones', 'code' => 'smartphones'],
            ['name' => 'Watch', 'code' => 'watch'],
        ]);
        self::showMessage('Наполнение таблицы category - успешно!');
    }

    public function fillProductDescriptionTable()
    {
        // Вставка данных в таблицу product_description
        DatabaseManager::table('product_description')->insert([
            [
                'product_id' => 1,
                'description' => 'Product 1 description'
            ],
            [
                'product_id' => 2,
                'description' => 'Product 2 description'
            ],
            [
                'product_id' => 3,
                'description' => 'Product 3 description'
            ],
            [
                'product_id' => 4,
                'description' => 'Product 4 description'
            ],
            [
                'product_id' => 5,
                'description' => 'Product 5 description'
            ],
            [
                'product_id' => 6,
                'description' => 'Product 6 description'
            ],
            [
                'product_id' => 7,
                'description' => 'Product 7 description'
            ],
            [
                'product_id' => 8,
                'description' => 'Product 8 description'
            ],
            [
                'product_id' => 9,
                'description' => 'Product 9 description'
            ],
            [
                'product_id' => 10,
                'description' => 'Product 10 description'
            ],
            [
                'product_id' => 11,
                'description' => 'Product 11 description'
            ],
            [
                'product_id' => 12,
                'description' => 'Product 12 description'
            ]
        ]);
        self::showMessage('Наполнение таблицы product_description - успешно!');
    }

    public function fillProductCharacteristicTable()
    {
        // Вставка данных в таблицу product_characteristic
        DatabaseManager::table('product_characteristic')->insert([
            [
                'product_id' => 1,
                'characteristic_id' => 1
            ],
            [
                'product_id' => 1,
                'characteristic_id' => 13
            ],
            [
                'product_id' => 2,
                'characteristic_id' => 2
            ],
            [
                'product_id' => 2,
                'characteristic_id' => 14
            ],
            [
                'product_id' => 3,
                'characteristic_id' => 3
            ],
            [
                'product_id' => 3,
                'characteristic_id' => 15
            ],
            [
                'product_id' => 4,
                'characteristic_id' => 4
            ],
            [
                'product_id' => 4,
                'characteristic_id' => 16
            ],
            [
                'product_id' => 5,
                'characteristic_id' => 5
            ],
            [
                'product_id' => 5,
                'characteristic_id' => 17
            ],
            [
                'product_id' => 6,
                'characteristic_id' => 6
            ],
            [
                'product_id' => 6,
                'characteristic_id' => 18
            ],
            [
                'product_id' => 7,
                'characteristic_id' => 7
            ],
            [
                'product_id' => 7,
                'characteristic_id' => 19
            ],
            [
                'product_id' => 8,
                'characteristic_id' => 8
            ],
            [
                'product_id' => 8,
                'characteristic_id' => 20
            ],
            [
                'product_id' => 9,
                'characteristic_id' => 9
            ],
            [
                'product_id' => 9,
                'characteristic_id' => 21
            ],
            [
                'product_id' => 10,
                'characteristic_id' => 10
            ],
            [
                'product_id' => 10,
                'characteristic_id' => 22
            ],
            [
                'product_id' => 11,
                'characteristic_id' => 11
            ],
            [
                'product_id' => 11,
                'characteristic_id' => 23
            ],
            [
                'product_id' => 12,
                'characteristic_id' => 12
            ],
            [
                'product_id' => 12,
                'characteristic_id' => 24
            ]
        ]);
        self::showMessage('Наполнение таблицы product_characteristic - успешно!');
    }

    public function fillBasketItemTable()
    {
        // Вставка данных в таблицу basket_item
        DatabaseManager::table('basket_item')->insert([
            [
                'user_id' => 1,
                'price' => 5030,
                'count' => 3,
                'user_session' => 'ef9466f4a0bfb0ca093ac2ba297c6789',
                'product_id' => 1,
            ],
            [
                'user_id' => 1,
                'price' => 3300,
                'count' => 1,
                'user_session' => 'ef9466f4a0bfb0ca093ac2ba297c6789',
                'product_id' => 3,
            ],
            [
                'user_id' => 1,
                'price' => 1070,
                'count' => 2,
                'user_session' => 'ef9466f4a0bfb0ca093ac2ba297c6789',
                'product_id' => 5,
            ]
        ]);
        self::showMessage('Наполнение таблицы basket_item - успешно!');
    }

    public function fillCharacteristicTable()
    {
        // Вставка данных в таблицу characteristic
        DatabaseManager::table('characteristic')->insert([
            ['id' => 1, 'code' => 'brand', 'name' => 'Apple', 'value' => 'apple'],
            ['id' => 2, 'code' => 'brand', 'name' => 'Apple', 'value' => 'apple'],
            ['id' => 3, 'code' => 'brand', 'name' => 'Lenovo', 'value' => 'lenovo'],
            ['id' => 4, 'code' => 'brand', 'name' => 'Sony', 'value' => 'sony'],
            ['id' => 5, 'code' => 'brand', 'name' => 'Meizu', 'value' => 'meizu'],
            ['id' => 6, 'code' => 'brand', 'name' => 'iPhone', 'value' => 'iPhone'],
            ['id' => 7, 'code' => 'brand', 'name' => 'iPhone', 'value' => 'iPhone'],
            ['id' => 8, 'code' => 'brand', 'name' => 'Xiaomi', 'value' => 'xiaomi'],
            ['id' => 9, 'code' => 'brand', 'name' => 'Xiaomi', 'value' => 'xiaomi'],
            ['id' => 10, 'code' => 'brand', 'name' => 'Xiaomi', 'value' => 'xiaomi'],
            ['id' => 11, 'code' => 'brand', 'name' => 'Watch', 'value' => 'watch'],
            ['id' => 12, 'code' => 'brand', 'name' => 'Watch', 'value' => 'watch'],
            ['id' => 13, 'code' => 'memory', 'name' => '16GB', 'value' => '16gb'],
            ['id' => 14, 'code' => 'memory', 'name' => '8GB', 'value' => '8gb'],
            ['id' => 15, 'code' => 'memory', 'name' => '8GB', 'value' => '8gb'],
            ['id' => 16, 'code' => 'memory', 'name' => '8GB', 'value' => '8gb'],
            ['id' => 17, 'code' => 'memory', 'name' => '16GB', 'value' => '16gb'],
            ['id' => 18, 'code' => 'memory', 'name' => '16GB', 'value' => '16gb'],
            ['id' => 19, 'code' => 'memory', 'name' => '8GB', 'value' => '8gb'],
            ['id' => 20, 'code' => 'memory', 'name' => '16GB', 'value' => '16gb'],
            ['id' => 21, 'code' => 'memory', 'name' => '8GB', 'value' => '8gb'],
            ['id' => 22, 'code' => 'memory', 'name' => '8GB', 'value' => '8gb'],
            ['id' => 23, 'code' => 'memory', 'name' => '16GB', 'value' => '16gb'],
            ['id' => 24, 'code' => 'memory', 'name' => '8GB', 'value' => '8gb']
        ]);
        self::showMessage('Наполнение таблицы characteristic - успешно!');
    }

    public function down(): void
    {
        // Удаление таблицы basket_item
        DatabaseManager::schema()->dropIfExists('basket_item');

        // Удаление таблицы product_characteristic
        DatabaseManager::schema()->dropIfExists('product_characteristic');

        // Удаление таблицы product_description
        DatabaseManager::schema()->dropIfExists('product_description');

        // Удаление таблицы category
        DatabaseManager::schema()->dropIfExists('category');

        // Удаление таблицы product
        DatabaseManager::schema()->dropIfExists('product');

        // Удаление таблицы users
        DatabaseManager::schema()->dropIfExists('users');

        // Удаление таблицы characteristic
        DatabaseManager::schema()->dropIfExists('characteristic');
    }
}