document.addEventListener("DOMContentLoaded", function () {
    var mySwiper = new Swiper('.image-slider',{
        navigation: {
            nextEl: '.swiper-button-next',
            prevEl: '.swiper-button-prev'
        },
        slidesPerView: 3,
        spaceBetween: 30,
        loop: true,
        loopAdditionalSlides: 1,
        autoplay: {
            delay: 3000, // Задержка между сменой слайдов в миллисекундах
            // disableOnInteraction: true // Отключить автоматическую прокрутку, когда пользователь взаимодействует со слайдером
        }
    });
    // получение элемента слайдера
    var sliderEl = document.querySelector('.swiper-container');

    // функция для остановки автоматической прокрутки
    function stopAutoplay() {
        mySwiper.autoplay.stop();
    }

    // функция для возобновления автоматической прокрутки
    function startAutoplay() {
        mySwiper.autoplay.start();
    }

    // добавление слушателей событий для остановки и возобновления автоматической прокрутки
    sliderEl.addEventListener('mouseenter', stopAutoplay);
    sliderEl.addEventListener('touchstart', stopAutoplay);
    sliderEl.addEventListener('mouseleave', startAutoplay);
    sliderEl.addEventListener('touchend', startAutoplay);
});