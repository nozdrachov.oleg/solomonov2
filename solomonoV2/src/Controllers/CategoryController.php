<?php

namespace Oleg\SolomonoV2\Controllers;

use Illuminate\Database\Capsule\Manager as DatabaseManager;

use mysql_xdevapi\Result;
use Oleg\SolomonoV2\App\Category;
use Oleg\SolomonoV2\App\Cache\CacheManager;
use Oleg\SolomonoV2\View\BaseView;
use Oleg\SolomonoV2\View\JsonView;
use Oleg\SolomonoV2\View\LatteView;

class CategoryController
{
    protected Category $category;
    protected CacheManager $cache;

    public function __construct()
    {
        $this->category = new Category();
        $this->cache = CacheManager::getInstance();
    }

    /**
     * @param array $params
     * @return BaseView
     */
    public function category(array $params)
    {
        $timeStart = microtime(true);

//        $client = new \Predis\Client([
//            'scheme' => 'tcp',
//            'host'   => 'redis-container',
//            'port'   => 6379,
//        ]);

        $categoryCode = $params['code'];
        $filtersString = $params['filters'] ?? '';
        $cacheKey = "{$categoryCode}|{$filtersString}";

//        if ($client->exists($cacheKey)) {
//            // Если данные есть в кеше, извлекаем их.
//            $result = unserialize($client->get($cacheKey));
//        } else {
            // Если данных нет в кеше, получаем их (например, из базы данных) и сохраняем в кеше.

            // Разбор сортировки
            $sortPattern = '/sort-([\w-]+)/';
            preg_match($sortPattern, $filtersString, $sortMatch);
            $sort = isset($sortMatch[1]) ? $sortMatch[1] : null;

            // Разбор фильтров
            $filterPattern = '/([a-zA-Z]+)-filtr_([\w-]+)/';
            preg_match_all($filterPattern, $filtersString, $matches, PREG_SET_ORDER);

            $filters = [];
            foreach ($matches as $match) {
                $filterCode = $match[1];
                $filterValues = explode('-or-', $match[2]);
                $filters[$filterCode] = $filterValues;
            }

//            $result['products'] = $this->category->getProducts($categoryCode, $filters, $sort);
//            $result['filters'] = $this->category->getFilters($categoryCode);
//            $result['categories'] = $this->category->getMenu();
//            $client->set($cacheKey, serialize($result), 'ex', 60);
//        }
        $result = $this->cache->getOrSetCache($cacheKey, function() use ($categoryCode, $filters, $sort) {
            return [
                'products' => $this->category->getProducts($categoryCode, $filters, $sort),
                'filters' => $this->category->getFilters($categoryCode),
                'categories' => $this->category->getMenu()
            ];
        });
        $timeEnd = microtime(true);
        $executionTime = ($timeEnd - $timeStart)/60;
        $result['executionTime'] = $executionTime;
        $view = new LatteView();
        $view->setData($result);
        $view->setTemplate('category.latte');
        return $view;
    }

    public function getFilteredProducts(array $params)
    {
        $categoryCode = $params['code'];
        $filtersString = $params['filters'] ?? '';

        $cacheKey = "{$categoryCode}|{$filtersString}+FILTER";
        // Разбор сортировки
        $sortPattern = '/sort-([\w-]+)/';
        preg_match($sortPattern, $filtersString, $sortMatch);
        $sort = isset($sortMatch[1]) ? $sortMatch[1] : null;

        // Разбор фильтров
        $filterPattern = '/([a-zA-Z]+)-filtr_([\w-]+)/';
        preg_match_all($filterPattern, $filtersString, $matches, PREG_SET_ORDER);

        $filters = [];
        foreach ($matches as $match) {
            $filterCode = $match[1];
            $filterValues = explode('-or-', $match[2]);
            $filters[$filterCode] = $filterValues;
        }

//        $products = $this->category->getProducts($categoryCode, $filters, $sort);

        $products = $this->cache->getOrSetCache($cacheKey, function() use ($categoryCode, $filters, $sort) {
            return $this->category->getProducts($categoryCode, $filters, $sort);
        });
        $view = new JsonView();
        $view->setData($products);
        return $view;
    }
}
