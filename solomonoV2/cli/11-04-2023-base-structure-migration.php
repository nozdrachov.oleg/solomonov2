<?php
use Oleg\SolomonoV2\Migrations\BaseStructureMigration;

require __DIR__.'/../vendor/autoload.php';
use Dotenv\Dotenv;

$dotenv = Dotenv::createImmutable(__DIR__.'/../..');
$dotenv->load();

try {
    $parameters = BaseStructureMigration::parseParameters($argv);

    // Создаем экземпляр класса BaseStructureMigration
    $baseStructureMigration = new BaseStructureMigration();
    $baseStructureMigration->execute($parameters);
} catch (Exception $e) {
    BaseStructureMigration::showMessage($e->getMessage());
}

