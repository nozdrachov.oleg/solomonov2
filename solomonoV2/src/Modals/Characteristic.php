<?php

namespace Oleg\SolomonoV2\Modals;

use Illuminate\Database\Eloquent\Model;

class Characteristic extends Model
{
    protected $table = 'characteristic';

    public function products()
    {
        return $this->belongsToMany(Product::class, 'product_characteristic');
    }
}
