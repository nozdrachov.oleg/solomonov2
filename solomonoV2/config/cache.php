<?php

return [
    'connect' => [
        'class' => '\Predis\Client',
        'scheme' => 'tcp',
        'host'   => 'redis-container',
        'port'   => 6379,
    ]
];