<?php

namespace Oleg\SolomonoV2\App;

use Oleg\SolomonoV2\Modals\Product;
use Oleg\SolomonoV2\Modals\Category as CategoryModel;

class Category
{
    public function getProducts(string $categoryCode, array $filters = [],  $sort = null): array
    {
        $query = Product::whereHas('category', function ($query) use ($categoryCode) {
            $query->where('code', $categoryCode);
        })->with(['category:id,code', 'characteristics:characteristic.id,code,name,value']);

        foreach ($filters as $filterCode => $filterValues) {
            $query->whereHas('characteristics', function ($query) use ($filterCode, $filterValues) {
                $query->where('code', $filterCode)->whereIn('value', $filterValues);
            });
        }

        if ($sort && $sort !== 'sort') {
            $query->orderBy($sort);
        }

        return $query->get()->toArray();
    }

    public function getFilters(string $categoryCode): array
    {
        $products = $this->getProducts($categoryCode);

        // Получаем доступные фильтры для данной категории
        $availableFilters = [];
        foreach ($products as $product) {
            foreach ($product['characteristics'] as $characteristic) {
                $code = $characteristic['code'];
                $value = $characteristic['value'];
                $name = $characteristic['name'];
                if (!isset($availableFilters[$code])) {
                    $availableFilters[$code] = [];
                }
                if (!isset($availableFilters[$code][$value])) {
                    $availableFilters[$code][$value] = $name;
                }
            }
        }

        return $availableFilters;
    }

    public function getMenu(): array
    {
        $categories = CategoryModel::select('code', 'name')->get()->toArray();
        foreach ($categories as &$item) {
            $item['url'] = '/category/' . $item['code'];
        }
        unset($item);
        return $categories;
    }
}
