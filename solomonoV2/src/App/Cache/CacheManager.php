<?php

namespace Oleg\SolomonoV2\App\Cache;

use Oleg\SolomonoV2\App\Helpers\Singleton;

class CacheManager extends Singleton
{
    protected $client;

    protected function __construct()
    {
        $this->connect();
    }

    protected function connect()
    {
        $cacheConfig = require __DIR__.'/../../../config/cache.php';
        $source = env('CACHE_SOURCE');
        $this->client = new $cacheConfig[$source]['class']([
            'scheme' => $cacheConfig[$source]['scheme'],
            'host'   => $cacheConfig[$source]['host'],
            'port'   => $cacheConfig[$source]['port'],
        ]);
    }

    public function getOrSetCache($cacheKey, callable $callback)
    {
        if ($this->client->exists($cacheKey)) {
            // Если данные есть в кеше, извлекаем их.
            $result = unserialize($this->client->get($cacheKey));
        } else {
            $result = $callback();
            $this->client->set($cacheKey, serialize($result), 'ex', 60);
        }

        return $result;
    }
}