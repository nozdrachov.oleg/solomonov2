<?php

//return [
//    'hit' => [
//        [
//            'class' => Monolog\Handler\StreamHandler::class,
//            'path' => __DIR__.'/../../storage/log/hit.log',
//            'level' => Monolog\Logger::INFO,
//        ],
//        [
//            'class' => Oleg\SolomonoV2\App\Log\CustomDatabaseHandler::class,
//            'path' => 'hit_logs',
//            'level' => Monolog\Logger::INFO,
//        ],
//    ],
//    'error' => [
//        [
//            'class' => Monolog\Handler\StreamHandler::class,
//            'path' => __DIR__.'/../../storage/log/error.log',
//            'level' => Monolog\Logger::ERROR,
//        ],
//        [
//            'class' => Oleg\SolomonoV2\App\Log\CustomDatabaseHandler::class,
//            'path' => 'error_logs',
//            'level' => Monolog\Logger::ERROR,
//        ],
//    ],
//];

return [
    'file' => [
        'class' => Monolog\Handler\StreamHandler::class,
        'path' => __DIR__.'/../../storage/log',
    ],
    'database' => [
        'class' => Oleg\SolomonoV2\App\Log\CustomDatabaseHandler::class,
        'table_template' => 'log_{name}',
    ]
];
