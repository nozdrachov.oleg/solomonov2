<?php

namespace Oleg\SolomonoV2\Controllers;

use Illuminate\Database\Capsule\Manager as DatabaseManager;
use Oleg\SolomonoV2\App\Category;
use Oleg\SolomonoV2\Modals\Product;
use Oleg\SolomonoV2\View\BaseView;
use Oleg\SolomonoV2\View\LatteView;

class IndexController
{
    protected Category $category;

    public function __construct()
    {
        $this->category = new Category();
    }

    public function index(): BaseView
    {
        $result['categories'] = $this->category->getMenu();
        $params = [
            'menu' => [
                'laptop' => 'Ноутбуки',
                'phone' => 'Телефоны',
                'watch' => 'Часы'
            ],
            'filter' => [
                'all' => 'Все',
                'laptops' => 'Ноутбуки',
                'phones' => 'Телефоны',
                'watch' => 'Часы'
            ],
            'test' => $result,
            'product' => [
                'mack' => [
                    'name' => 'mack',
                    'price' => '982$'
                ],
                'lenovo' => [
                    'name' => 'lenovo',
                    'price' => '1250$'
                ],
                'sony' => [
                    'name' => 'sony',
                    'price' => '1210$'
                ],
                'sony' => [
                    'name' => 'sony',
                    'price' => '1390$'
                ],
                'sony' => [
                    'name' => 'sony',
                    'price' => '430$'
                ],
                'sony' => [
                    'name' => 'sony',
                    'price' => '1961$'
                ]
            ]
        ];
        $view = new LatteView();
        $view->setData($params);
        $view->setTemplate('index.latte');
        return $view;
    }
}