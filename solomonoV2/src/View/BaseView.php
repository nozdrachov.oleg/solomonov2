<?php

namespace Oleg\SolomonoV2\View;

abstract class BaseView
{
    protected array $data = [];

    protected string $content;

    public function getContent(): string
    {
        return $this->content;
    }

    public function setData(array $data): void
    {
        $this->data = $data;
    }

    public abstract function renderContent(): void;
}