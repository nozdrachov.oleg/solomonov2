const UrlStateModule = (function () {
    const category = document.querySelector("#category").value;

    function parseFromUrl(pattern) {
        const currentUrl = window.location.href;
        const params = {};

        let match;
        while ((match = pattern.exec(currentUrl)) !== null) {
            const paramName = match[1];
            const paramValues = match[2].split("-or-");
            params[paramName] = paramValues;
        }

        return params;
    }

    function updateProducts(products) {
        const productContainer = document.querySelector("#all-products-container");
        productContainer.innerHTML = "";

        if (Array.isArray(products)) {
            products.forEach((product) => {
                const productElement = document.createElement("div");
                productElement.classList.add("product-card");

                productElement.innerHTML = `
                <a href="#" class="product-image">
                    <img src="/img/smarfon.png" alt="${product.name}">
                </a>
                <div class="product-info">
                    <span class="availability">В наличии</span>
                    <div class="price-and-button">
                        <p class="price">${product.price}$</p>
                        <button class="buy-button">Купить</button>
                    </div>
                    <div class="product-name">
                        <a href="#">${product.name}(гб)</a>
                    </div>
                </div>
            `;
                productContainer.appendChild(productElement);
            });
        } else {
            console.error('Products is not an array:', products);
        }
    }

    function AjaxFromUrl(filterUrl) {
        fetch(filterUrl + '/ajax', {
            // headers: {
            //     'Accept': 'application/json',
            //     'X-Requested-With': 'XMLHttpRequest'
            // }
        })
            .then((response) => {
                if (!response.ok) {
                    throw new Error('Network response was not ok');
                }
                return response.json();
            })
            .then((products) => {
                updateProducts(products);
            })
            .catch((error) => {
                console.error("Error fetching filtered products:", error);
                console.error("Error fetching filtered products:", error);
                console.error("Response data:", error.response.data);
            });
    }

    return {
        parseFromUrl: parseFromUrl,
        category: category,
        AjaxFromUrl: AjaxFromUrl,
    }
})();

const FilterModule = (function () {
    let filters;

    function init() {
        filters = document.querySelectorAll(".filter");
        const savedFilters = UrlStateModule.parseFromUrl(/([a-zA-Z]+)-filtr_([\w-]+)/g);
        if (Object.keys(savedFilters).length > 0) {
            applySavedFilters(savedFilters);
        }
        filters.forEach((filter) => {
            filter.addEventListener("change", submitFilters)
        });
    }

    function applySavedFilters(savedFilters) {
        filters.forEach((filter) => {
            const filterName = filter.getAttribute("data-filter");
            const savedValues = savedFilters[filterName] || [];

            filter.querySelectorAll("input[type=checkbox]").forEach((checkbox) => {
                checkbox.checked = savedValues.includes(checkbox.value);
            });
        });
    }

    function submitFilters() {
        const selectedFilters = getSelectedFilters();
        const filterUrl = buildFilterUrl(selectedFilters);
        updateUrlAndSaveState(filterUrl);
        UrlStateModule.AjaxFromUrl(filterUrl);

    }

    function getSelectedFilters() {
        let selectedFilters = {};

        filters.forEach((filter) => {
            const filterName = filter.getAttribute("data-filter");
            const checkedOptions = Array.from(filter.querySelectorAll("input:checked")).map(
                (input) => input.value
            );

            if (checkedOptions.length) {
                selectedFilters[filterName] = checkedOptions;
            }
        });

        return selectedFilters;
    }

    function buildFilterUrl(selectedFilters) {
        const category = UrlStateModule.category;
        const sortObj = UrlStateModule.parseFromUrl(/(sort)-([^\/]+)/g);
        //******* сравнить заработает или нет (с таким же)
        const sortPart = sortObj.sort ? `/sort-${sortObj.sort[0]}` : '';
        const filterStrings = Object.entries(selectedFilters).map(
            ([key, values]) => `${key}-filtr_${values.join("-or-")}`
        );

        return `/category/${category}${sortPart}${filterStrings.length > 0 ? '/' + filterStrings.join('/') : ''}`;
    }

    function updateUrlAndSaveState(filterUrl) {
        history.pushState({ filters : getSelectedFilters() }, "", filterUrl);

        window.addEventListener('popstate', function(event) {
            if (event.state && event.state.filters) {
                applySavedFilters(event.state.filters);
                submitFilters();
            }
        });
    }

    return {
        init: init,
    };
})();

const SortModule = (function () {
    let sort;

    function init() {
        sort = document.querySelector(".sort select");
        const savedSort = UrlStateModule.parseFromUrl(/(sort)-([^\/]+)/g);
        if (Object.keys(savedSort).length > 0) {
            //узнать почему прото savedSort не работает , а почему именно savedSort.sort(там же 1 обьект почему он его
            // прото не подхватывает почему именно свойство надо указывать , если 1 одно почему как с массивом не работает
            applySavedSort(savedSort.sort);
        }
        sort.addEventListener("change", submitSort);
    }

    function applySavedSort(savedSort) {
        //******* сравнить заработает или нет (с таким же)
        if (savedSort) {
            sort.value = savedSort;
        }
    }

    function submitSort() {
        const selectedSort = getSelectedSort();
        const sortUrl = buildSortUrl(selectedSort);
        updateUrlAndSaveState(sortUrl);
        UrlStateModule.AjaxFromUrl(sortUrl);
    }

    function getSelectedSort() {
        let selectedSort = {};
        selectedSort['sort'] = sort.value;

        return selectedSort
    }

    function buildSortUrl(selectedSort) {
        const category = UrlStateModule.category;
        const filterObj = UrlStateModule.parseFromUrl(/([a-zA-Z]+)-filtr_([\w-]+)/g);
        const filterStrings = Object.entries(filterObj).map(
            ([key, values]) => `${key}-filtr_${values.join("-or-")}`
        );
        const sortPart = selectedSort.sort ? `/sort-${selectedSort.sort}` : '';

        return `/category/${category}${sortPart}${filterStrings.length > 0 ? '/' + filterStrings.join('/') : ''}`;
    }

    function updateUrlAndSaveState(sortUrl) {
        history.pushState({ sort : getSelectedSort() }, "", sortUrl);

        window.addEventListener('popstate', function(event) {
            if (event.state && event.state.sort) {
                applySavedSort(event.state.sort);
                submitSort();
            }
        });
    }

    return {
        init: init,
    };
})();

document.addEventListener("DOMContentLoaded", function () {
    FilterModule.init();
    SortModule.init();
});