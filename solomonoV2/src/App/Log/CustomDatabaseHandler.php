<?php

namespace Oleg\SolomonoV2\App\Log;

use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;
use Illuminate\Database\Capsule\Manager as DatabaseManager;

class CustomDatabaseHandler extends AbstractProcessingHandler
{
    protected string $table;

    public function __construct(string $table, $level = Logger::DEBUG, bool $bubble = true)
    {
        $this->table = $table;
        parent::__construct($level, $bubble);

    }

    protected function createTable(): void
    {
        if (!DatabaseManager::schema()->hasTable($this->table)) {
            DatabaseManager::schema()->create($this->table, function ($table) {
                $table->increments('id');
                $table->integer('level');
                $table->text('message');
                $table->text('context');
                $table->timestamp('created_at');
            });
        }
    }

    protected function write(array $record): void
    {
        $this->createTable();
        DatabaseManager::table($this->table)->insert([
            'level' => $record['level'],
            'message' => $record['message'],
            'context' => json_encode($record['context']),
            'created_at' => $record['datetime']->format('Y-m-d H:i:s'),
        ]);
    }
}