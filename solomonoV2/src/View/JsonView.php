<?php

namespace Oleg\SolomonoV2\View;

class JsonView extends BaseView
{
    public function renderContent(): void
    {
        $this->content = json_encode($this->data);
    }
}