<?php
return [
    [
        'http_method' => 'GET',
        'route' => '/category/{code}{filters:/?(?:sort-(?:[^/]+))?/?(?:[a-zA-Z]+-filtr_(?:[^/]+)
        (?:-or-[^/]+)*)*(?:/(?:[a-zA-Z]'. '+-filtr_(?:[^/]+)(?:-or-[^/]+)*))*/?}',
        'handler' => [
            'class' => \Oleg\SolomonoV2\Controllers\CategoryController::class,
            'method' => 'category',
            'dependencies' => [
                'scripts' => [
                    '/js/category.js'
                ],
                'styles' => [
                    '/css/category.css'
                ]
            ],
        ],
    ],
    [
        'http_method' => 'GET',
        'route' => '/category/{code}{filters:/?(?:sort-(?:[^/]+))?/?(?:[a-zA-Z]+-filtr_(?:[^/]+)
        (?:-or-[^/]+)*)*(?:/(?:[a-zA-Z]'. '+-filtr_(?:[^/]+)(?:-or-[^/]+)*))*/?}/ajax',
        'handler' => [
            'class' => \Oleg\SolomonoV2\Controllers\CategoryController::class,
            'method' => 'getFilteredProducts'
        ]
    ],
    [
        'http_method' => 'GET',
        'route' => '/',
        'handler' => [
            'class' => \Oleg\SolomonoV2\Controllers\IndexController::class,
            'method' => 'index',
            'dependencies' => [
                'scripts' => [
                    '/js/index.js'
                ],
                'styles' => [
                    '/css/index.css'
                ]
            ]
        ]
    ]
];