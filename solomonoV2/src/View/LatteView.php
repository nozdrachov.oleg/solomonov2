<?php

namespace Oleg\SolomonoV2\View;

use Latte\Engine as LatteEngine;

class LatteView extends BaseView
{
    private LatteEngine $latteEngine;
    private string $cachePath;
    private string $templatePath;
    private string $template;


    public function __construct()
    {
        $this->cachePath = "{$_SERVER['DOCUMENT_ROOT']}/../cache/latte";
        $this->templatePath = "{$_SERVER['DOCUMENT_ROOT']}/../template";
        $this->latteEngine = new LatteEngine;
        $this->latteEngine->setTempDirectory($this->cachePath);
    }

    public function renderContent(): void
    {
        //как наполняется content если ничего не возвращается
        $this->content = $this->latteEngine->renderToString(
            "{$this->templatePath}/{$this->template}", $this->data
        );
    }

    public function setTemplate(string $template): void
    {
        $this->template = $template;
    }

    public function addDependency(string $dependency, string $key): void
    {
        $this->data[$key][] = $dependency;
    }
}